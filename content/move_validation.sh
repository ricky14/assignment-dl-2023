#!/bin/bash


while read -r imageid;
do
    if [ $imageid != "" ]
    then
        mv "./train_data/labels/$imageid.txt" "./validation_data/labels//$imageid.txt"
        mv "./train_data/images/$imageid.jpg" "./validation_data/images/$imageid.jpg"
    fi

done < "./validation_data/validation_split.txt"

