#!/bin/bash


while read -r imageid;
do
    if [ $imageid != "" ]
    then
        rm -f "./train_data/labels/$imageid.txt"
        while IFS="," read -r left top right bottom class steamx steamy
        do
            #echo "l: $left"
            #echo "t: $top"
            #echo "r: $right"
            #echo "b: $bottom"
            #echo "c: $class"

            echo "print($class, (($right+$left)/2)/1920.0, (($bottom+$top)/2)/1088.0, ($right-$left)/1920.0, ($bottom-$top)/1088.0)" | python >> "./train_data/labels/$imageid.txt"
            #echo "print((($bottom+$top)/2)/1088.0)" | python
            #echo "print(($right-$left)/1920.0)" | python
            #echo "print(($bottom-$top)/1088.0)" | python
        done < "./train_data/bboxes/CropOrWeed2/$imageid.csv"
    fi

done < "./train_data/train_split.txt"

while read -r imageid;
do
    if [ $imageid != "" ]
    then
        rm -f "./test_data/labels/$imageid.txt"
        while IFS="," read -r left top right bottom class steamx steamy
        do

            echo "print($class, (($right+$left)/2)/1920.0, (($bottom+$top)/2)/1088.0, ($right-$left)/1920.0, ($bottom-$top)/1088.0)" | python >> "./test_data/labels/$imageid.txt"
            
        done < "./test_data/bboxes/CropOrWeed2/$imageid.csv"
    fi

done < "./test_data/test_split.txt"

