#!/bin/bash


while read -r imageid;
do
    if [ $imageid != "" ]
    then
        cp -u "/data/bboxes/CropOrWeed2/$imageid.csv" "test_data/bboxes/CropOrWeed2/$imageid.csv"
        cp -u "/data/labelIds/CropOrWeed2/$imageid.png" "test_data/labelIds/CropOrWeed2/$imageid.png"
        cp -u "/data/images/$imageid.jpg" "test_data/images/$imageid.jpg"
        #cp "./cropandweed-dataset/data/params/$imageid.csv" "./test_data/params/$imageid.csv"
    fi

done < "./test_data/test_split.txt"

while read -r imageid;
do
    if [ $imageid != "" ]
    then
        cp -u "/data/bboxes/CropOrWeed2/$imageid.csv" "train_data/bboxes/CropOrWeed2/$imageid.csv"
        cp -u "/data/labelIds/CropOrWeed2/$imageid.png" "train_data/labelIds/CropOrWeed2/$imageid.png"
        cp -u "/data/images/$imageid.jpg" "train_data/images/$imageid.jpg"
        #cp "./cropandweed-dataset/data/params/$imageid.csv" "./train_data/params/$imageid.csv"
    fi

done < "./train_data/train_split.txt"
